This repository includes the code for the training and inference of our sentiment analysis models. Using the source/v5/inference.ipynb notebook, predictions for user-inputs can be made. The notebook source/comparison.ipynb was used for the comparison of the model from Guhr et al. with our model on 18 example sentences.

v1 : Bag-of-words and bag-of-n-grams

v2 : LSTM and CNN

v3 : LSTM and CNN with fastText embeddings

v4 : Feature based fine-tuning of GBERT base and GBERT large

v5 : Fine-tuning of GBERT base, GBERT large, and GELECTRA large
